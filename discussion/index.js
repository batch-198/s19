console.log("Hello world");

// Conditional Statements

// Conditional statements allows us to perform tasks based on a condition

let num1 = 0;

// If statement - if statement allow us to perform a task IF the condition given is true.

/*

if(condition){
	task/code to perform
}

*/

// runs code if the condition is true
if(num1 === 0){
	console.log("the value of num1 is 0");
}

num1 = 25

// code does not run because the condition is now false
if(num1 === 0){
	console.log("the current value of num1 is still 0");
}

let city = "New York";

if(city === "New Jersey"){
	console.log("Welcome to New Jersey");
}

// Else statement executes a code/task if the previous condition/s are not met

 if(city === "New Jersey"){
	console.log("Welcome to New Jersey");
}	else {
	console.log("This is not New Jersey!");
}

// else statement will be run
if(num1 < 20){
	console.log("num1's value is less than 20");
} else {
	console.log("num1's value is more than 20");
}

// We can use if-else in a function for reusability.

function cityChecker(city){

	if(city === "New York"){
		console.log("Welcome to the Empire State!");
	} else{
		console.log("You're not in New York!");
	}
}

cityChecker("New York");
cityChecker("Los Angeles");

function budgetChecker(amount){
	if(amount <= 40000){
		console.log("You're still within budget");
	} else{
		console.log("You are currently over the budget");
	}
}

budgetChecker(20000);
budgetChecker(41000);

// Else If
// Allows us to execute code/task if the previous condition/s are not met or false, and IF the specified condition is met instead

let city2 = "Manila";

if(city2 === "New York"){
	console.log("Welcome to New York!");
} else if (city2 === "Manila"){
	console.log("Welcome to Manila!")
} else {
	console.log("I don't know where you are.")
}

// In a conditional chain statement, IF statements are needed. Meanwhile ELSE and ELSE IF are optional

// Usually we only have a single else statement. Because Else is run when ALL conditions have not been met.

let role = "admin";

if(role === "developer"){
	console.log("Welcome back, Developer");
} else if(role === "admin"){
	console.log("Hello, Admin");
} else{
	console.log("Role provided is invalid");
}

// Multiple Else If Statements

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return "Not a typhoon yet."
	} else if(windSpeed <= 61){
		return "Tropical Depression Detected"
	} else if(windSpeed >=62 && windSpeed <=88){
		return "Tropical Storm Detected."
	} else if(windSpeed >= 89 && windSpeed <= 117){
		return "Severe Tropical Storm Detected."
	} 	else {
		return "Typhoon detected."
	}
}

let typhoonMessage1 = determineTyphoonIntensity(29);
let typhoonMessage2 = determineTyphoonIntensity(62);
let typhoonMessage3 = determineTyphoonIntensity(61);
let typhoonMessage4 = determineTyphoonIntensity(88);
let typhoonMessage5 = determineTyphoonIntensity(117);
let typhoonMessage6 = determineTyphoonIntensity(120);

console.log(typhoonMessage1);
console.log(typhoonMessage2);
console.log(typhoonMessage3);
console.log(typhoonMessage4);
console.log(typhoonMessage5);
console.log(typhoonMessage6);

/*Truthy and Falsy values*/

// In JS, there are values that are considered "truthy", which means in a boolean context, like determining an IF condition, it is considered true.

// Samples of Truthy
// 1. true
// 2. 
if(1){
	console.log("1 is truthy");
}
// 3.
if([]){
	// even though array is empty, it is already an existing instance of an array
	console.log("[] empty array is truthy");
}

// Falsy value are considered "false" in a boolean context like determining an IF condition
// 1. false
// 2.
if(0){
	console.log("0 is falsy.");
}
// 3.
if(undefined){
	console.log("undefined is not falsy");
} else{
	console.log("undefined is falsy");
}

// Conditional Ternary Operator
// Ternary Operator is used as a shorted alternative to IF ELSE statements
// It is also able to implicitly return a value. Meaning it does not have to use a return keyword to return a value.

/*Syntax:
	(condition) ? ifTrue : ifFalse;
*/

let age = 17;
let result = age < 18 ? "Underage" : "Legal Age";
console.log(result);

/*let result2 = if(age < 18){
	return "Underage";
} else {
	return "Legal Age";
}

console.log(result2);

^ Result - Error*/

// Switch Statement
// Evaluate an expression and match the expression to a case clause
// An expression will be compared against different cases. Then, we will be able to run code IF the expression being evaluated matches a case.
// It is used alternatively from an IF ELSE statement. However, IF ELSE statements provides more complexity in its conditions.
// .toLowerCase method is a built-in JS method which converts a string to lowercase.

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

// Switch statement to evaluate the current day and shows a message that tell the user the color of the day.

// If the switch statement was not able to match a case with evaluated expression, it will run the default case.
// Break keyword ends the case's statement

switch(day){
	case 'monday':
		console.log("The color of the day is Red.");
		break;
	case 'tuesday':
		console.log("The color of the day is Orange.");
		break;
	case 'wednesday':
		console.log("The color of the day is Yellow.");
		break;
	case 'thursday':
		console.log("The color of the day is Purple.");
		break;
	case 'friday':
		console.log("The color of the day is Green.");
		break;
	case 'saturday':
		console.log("The color of the day is Black.")
		break;
	case 'sunday':
		console.log("The color of the day is White.")
		break;
	default:
		console.log("Please enter a valid day");
		break;
}	

// Try-Catch-Finally
// We use the try-catch-finally statements to catch errors, display, and inform about the error and continue the code instead of stopping
// We could also use the try-catch-finally statements to product our own error messages in the event of an error
// try-catch-finally is used to anticipate, catch, and inform about an error

// try allows us to run a code, if there is an error in the instance of our code, then we will be able to catch that error in our catch statement
try{
	alert(determineTyphoonIntensity(50))
// the error is passed into the catch statement. Developers usually name the error as error, err or even e
} catch (error){

	// with the use of typeof keyword, we will be able to return string which contains information what data type our error is
	// console.log(typeof error);
	// console.log(error);
	console.log(error.message);

} finally {

	// finally statement will run regardless of the success or failure of the try statement

	alert("Intensity Updates will show in a new alert");
}

// with a try catch statement, we can catch errors instead of stopping the whole program, we can catch the error and continue.

// this is what we call Error Handling

console.log("Will we continue to the next code?");